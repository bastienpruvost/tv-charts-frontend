import { GChart } from 'vue-google-charts'
import axios from 'axios'

export default {
  name: 'App',
  components: {
    GChart
  },
  data() {
    return {
      the_chart: null,
      landing: false,
      errorEmoji: false,
      currentCarouselOption: 0,
      show_param: new URL(location.href).searchParams.get('show'),
      api_data: {},
      filters: {
        show_name: "",
        zoomed_in: true,
        selected_season: null,
        showing: {
          trakt: true,
          imdb: true,
          tmdb: false,
        },
        smooth_curve: false,
        trendlines: false,
        season_view: false
      }
    }
  },
  mounted() {
    if (this.show_param) {
      axios.get(`http://localhost:3000/search/${this.show_param}`)
        .then((res) => { 
          this.api_data = res.data 
          preloadImages(this.api_data);
        })
        .catch((error) => {
          this.errorEmoji = true;
          if (error.response && error.response.status == "404") {
            document.getElementById('errorTitle').innerHTML  = "Not found"
            document.getElementById('loading_msg').innerHTML = "No results could be found for this show. Try another one!"
          }
          else {
            document.getElementById('errorTitle').innerHTML  = "Oh no!"
            document.getElementById('loading_msg').innerHTML = "An error occured. Please try again soon.<br />If the problem persists, feel free to contact me (link in the sidebar)"
          }
        })
      if (window.innerWidth < 900) {
        this.toggleSidebar();
      }
    } else {
      this.landing = true;
    }
    mapEnterKeyToButtonOnSearchBar();
    startCarousel(this.carouselOptions, this.currentCarouselOption);
  },
  computed: {
    getHViewWindow() {
      if (this.filters.selected_season) {
        return {
          min: getFirstEpiInSeason(this.api_data.episodes, this.filters.selected_season)-0.5,
          max: getLastEpiInSeason(this.api_data.episodes, this.filters.selected_season)+0.5
        }
      }
      return {
        min: 0.5,
        max: this.api_data.episodes.length + 0.5
      }
    },
    getVViewWindow() {
      var episodes = this.api_data.episodes
      var zoomed_in = this.filters.zoomed_in
      return {
        min: (zoomed_in ? (Math.max(0 , getMinRating(episodes, this.filters) - 0.2)) : 0),
        max: (zoomed_in ? (Math.min(10, getMaxRating(episodes, this.filters) + 0.2)) : 10)
      }
    },
    availableSeasons() {
      var formatted = []
      if (this.api_data.episodes) {
        var all_seasons = this.api_data.episodes.map((epi) => {return epi.season})
        var seasons = [...new Set(all_seasons)] // _.uniq
        formatted = seasons.map((s) => {
          return {
            value: s,
            text: "Season " + s
          }
        })
      }
      return [{value: null, text: 'All seasons'}].concat(formatted)
    },
    carouselOptions() {
      return [
        'Breaking Bad',
        'Buffy the Vampire Slayer',
        'Mr Robot',
        'Rick and Morty',
        'Six Feet Under',
        'Avatar: The Last Airbender',
        'Firefly',
        'American Gods',
        'Game of Thrones'
      ]
    },
    cssProps() {
      return {
        '--main_color':  '#1c1c1c',
        '--sec_color':   '#2a425c',
        '--grid_color':  '#0C1A2E',
        '--text_color':  '#F6F9FF',
        '--tmdb_color':  '#01b4c4',
        '--trakt_color': '#d70000',
        '--imdb_color':  '#FCBA03'
      }
    },
    chartData() {
      if (this.api_data.episodes) {
        var header = [
          [
            'episodeNumber', 
            {'type': 'string', 'role': 'tooltip', 'p': {'html': true}}, 
            'traktRating',
            'tmdbRating',
            'imdbRating'
          ]
        ]
        var data_episodes = this.api_data.episodes.map((epi, index) => {
          var minY = this.getVViewWindow.min
          return [
            index+1, 
            generateTooltipForEpisode(epi),
            getDisplayRatingForEpi(epi, this.api_data.season_averages, this.filters, 'trakt', minY),
            getDisplayRatingForEpi(epi, this.api_data.season_averages, this.filters, 'tmdb', minY),
            getDisplayRatingForEpi(epi, this.api_data.season_averages, this.filters, 'imdb', minY)
          ]
        })
        return header.concat(data_episodes)
      }
    },
    chartOptions() {
      if (this.api_data.episodes) {
        return {
          animation: {
            duration: 800,
            easing: 'inAndOut',
            startup: true
          },

          axisTitlesPosition: 'out',
          backgroundColor: this.cssProps['--sec_color'],

          chartArea: {
            height: '90%',
            width: '90%'
          },

          curveType: ((this.filters.season_view || !this.filters.smooth_curve ) ? 'none' : 'function'),

          focusTarget: 'category', // This line makes the entire category's tooltip active.
          
          tooltip: { 
            isHtml: true,
            trigger: 'both'
          },

          hAxis: {
            gridlines : {
              count: 0,
              color: this.cssProps['--grid_color']
            },
            ticks: getSeasonSeparators(this.api_data.episodes),

            viewWindow: this.getHViewWindow,

            textStyle: {
              color: this.cssProps['--text_color']
            }
          },

          lineWidth: (this.filters.season_view ? 2 : (this.filters.trendlines ? 0 : 1)),

          legend: {
            position: 'none'
          },

          pointSize: (this.filters.season_view ? 0 : 4),

          trendlines: {
            0: {
              type: 'linear',
              color: this.cssProps['--trakt_color'],
              lineWidth: (this.filters.trendlines ? 4 : 0),
              opacity: 0.5,
              showR2: false,
              visibleInLegend: false,
              pointsVisible:false
            },
            1: {
              type: 'linear',
              color: this.cssProps['--tmdb_color'],
              lineWidth: (this.filters.trendlines ? 4 : 0),
              opacity: 0.5,
              showR2: false,
              visibleInLegend: false,
              pointsVisible:false
            },
            2: {
              type: 'linear',
              color: this.cssProps['--imdb_color'],
              lineWidth: (this.filters.trendlines ? 4 : 0),
              opacity: 0.5,
              showR2: false,
              visibleInLegend: false,
              pointsVisible:false
            }
          },

          vAxis: {
            baseline: (this.getVViewWindow.min + this.getVViewWindow.max)/2,
            baselineColor: 'transparent',
            gridlines: {
              color: this.cssProps['--grid_color'],
              interval: 1
            },
            minorGridlines: {
              interval: 0.5
            },
            textStyle: {
              color: this.cssProps['--text_color']
            },
            viewWindow: this.getVViewWindow
          },
          colors: [this.cssProps['--trakt_color'], this.cssProps['--tmdb_color'], this.cssProps['--imdb_color']]
        }
      }
    },
    trakt_link() {
      return `https://trakt.tv/shows/${this.api_data.trakt_id}`
    },
    tmdb_link() {
      return `https://www.themoviedb.org/tv/${this.api_data.tmdb_id}`
    },
    imdb_link() {
      return `https://www.imdb.com/title/${this.api_data.imdb_id}`
    }
  },
  methods: {
    redirect: (show_name) => {
      if (show_name) {
        window.location.replace(`?show=${show_name}`);
      }
    },
    onChartReady: (chart) => {
      document.the_chart=chart
    },
    toggleSidebar: () => {
      var sb = document.getElementById("sidebar");
      sb.style.display = (sb.style.display == 'none') ? 'block' : 'none';
      window.dispatchEvent(new Event('resize'))
    }
  }
}

function mapEnterKeyToButtonOnSearchBar() {
  document.getElementById("homeSearchBar").addEventListener("keyup", function(event) {
    if (event.keyCode === 13) {
      event.preventDefault();
      document.getElementById("homeLetsgo").click();
    }
  })
  document.getElementById("searchBar").addEventListener("keyup", function(event) {
    if (event.keyCode === 13) {
      event.preventDefault();
      document.getElementById("letsgo").click();
    }
  })
}

function startCarousel(options, currentCarouselOption) {
  window.setInterval( () => {
    var div = document.getElementById("carousel");
    div.style.opacity=0
    window.setTimeout( () => {
      currentCarouselOption += 1
      if (currentCarouselOption >= options.length) {
        currentCarouselOption = 0
      }
      var link = document.getElementById("carouselShow");
      var val = options[currentCarouselOption];
      link.innerHTML = val;
      link.setAttribute("href", "./?show="+val)
      div.style.opacity=1
    }, 500)
  },3000)
}

function preloadImages(api_data) {
  api_data.episodes.forEach((epi) => {
    document.getElementById('hidden_div').innerHTML += `<img height='1' src="${epi.img_path}"/>`;
  })  
}

function getSeasonSeparators(episodes) {
  var current_season = episodes[0].season
  var separators = [{v: 0.5, f: 'S.1'}]
  episodes.forEach((epi, index) => {
      if (epi.season > current_season) {
        current_season++;
        separators.push({v: index+0.5, f: 'S.'+epi.season})
      }
    }
  );
  separators.push({v: episodes.length+0.5, f: ''})
  return separators
}

function getFirstEpiInSeason(episodes, season) {
  return episodes.findIndex((epi) => {
    return epi.season == season
  })+1
}

function getLastEpiInSeason(episodes, season) {
  var reverseCopy = episodes.slice().reverse(); // because reverse modifies the original array
  var last = reverseCopy.find((epi) => {
    return epi.season == season
  })
  return episodes.indexOf(last)+1
}

function getMinRating(episodes, filters) {
  return Math.min(...getAllDisplayedRatings(episodes, filters))
}

function getMaxRating(episodes, filters) {
  return Math.max(...getAllDisplayedRatings(episodes, filters))
}

function getAllDisplayedRatings(episodes, filters) {
  return episodes.flatMap((epi) => {
    var epi_displayed_ratings = []
    if (filters.showing['trakt']) { epi_displayed_ratings.push(epi.ratings.trakt) }
    if (filters.showing['tmdb']) { epi_displayed_ratings.push(epi.ratings.tmdb) }
    if (filters.showing['imdb']) { epi_displayed_ratings.push(epi.ratings.imdb) }
    if (epi_displayed_ratings == []) { return [1.0, 9.0]}
    return epi_displayed_ratings
  })
}

function getDisplayRatingForEpi(epi, season_averages, filters, data_source, minY) {
  if (filters.showing[data_source]) {
    if (filters.season_view) {
      return parseFloat(season_averages[data_source][epi.season])
    } else {
      return parseFloat(epi.ratings[data_source])
    }
  } else {
    return minY -1
  }
}

function generateTooltipForEpisode(val) {
  return '\
  <div style="padding:5px; width: 300px !important;">\
    <div style="text-align:left">\
    <span>Tip: click to anchor this</span>\
    <span style="float:right; font-size:14px; cursor:pointer;" onclick="document.the_chart.setSelection(null);">X</span>\
    </div>\
    <img style="height: 200px; max-width:280px;" src="' + val.img_path + '" />\
    <div style="font-size:18px; font-weight: bold;">\
      ' + val.season + '.' + val.number + ' - ' + val.title +'\
    </div>\
    <div>\
      Trakt rating: '+ val.ratings.trakt + ' \
      - <a target="_blank" href="' + val.links.trakt + '">Go</a>\
    </div>\
    <div>\
      TMDB rating: '+ val.ratings.tmdb + '\
      '+ ((val.links.tmdb) ? '- <a target="_blank" href="' + val.links.tmdb + '">Go</a>' : '') + '\
    </div>\
    <div>\
      IMDB rating: '+ val.ratings.imdb + '\
      - <a target="_blank" href="' + val.links.imdb + '">Go</a>\
    </div>\
  </div>'
}
